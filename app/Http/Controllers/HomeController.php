<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $purchased_courses = [];
        if (auth()->check()) {
            $purchased_courses = Course::whereHas('students', function($query) {
                $query->where('users.id', auth()->id());
            })
            ->with('lessons')
            ->orderBy('id', 'desc')
            ->get();
        }

        $totalcourses = Course::all()->count();
        $totaluser = User::all()->count();
        $courses =  Course::where('published', 1)->latest()->get();
        $latestcourse = Course::latest()->take(3)->get();

        return view('pages.beranda', compact('courses','purchased_courses','latestcourse', 'totalcourses', 'totaluser'));
    }
}
