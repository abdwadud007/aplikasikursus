<aside class="left-sidebar" data-sidebarbg="skin6">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link"
                                href="#" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span
                                    class="hide-menu">Dashboard</span></a></li>
                        @can('course_access')
                            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link {{ request()->is('admin/courses') || request()->is('admin/courses/*') ? 'active' : '' }}"
                                href="{{ route('admin.courses.index') }}" aria-expanded="false"><i 
                                class="mdi mdi-library"></i><span class="hide-menu">Kursus</span></a></li>
                        @endcan
                        @can('lesson_access')
                            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link {{ request()->is('admin/lessons') || request()->is('admin/lessons/*') ? 'active' : '' }}"
                                    href="{{ route('admin.lessons.index') }}" aria-expanded="false"><i class="mdi mdi-library-books"></i><span
                                        class="hide-menu">Materi</span></a></li>
                        @endcan
                        @can('user_access')
                            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link {{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}"
                                href="{{ route('admin.users.index') }}" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span
                                    class="hide-menu">Data User</span></a></li>

                        @endcan
                        <li class="sidebar-item"> <a  onclick="getElementById('logout-form').submit()" class="sidebar-link waves-effect waves-dark sidebar-link"
                                href="#" aria-expanded="false"><i class="mdi mdi-logout"></i><span
                                    class="hide-menu">Log Out</span></a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="post">
                                        @csrf 
                                    </form>
                        </li>
                    </ul>

                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>