@extends('dashboard')

@section('dashboard')
    <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-6">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-0 d-flex align-items-center">
                              <li class="breadcrumb-item"><a href="index.html" class="link"><i class="mdi mdi-home-outline fs-4"></i></a></li>
                              <li class="breadcrumb-item active" aria-current="page">Data Kursus</li>
                            </ol>
                          </nav>
                        <h1 class="mb-0 fw-bold">Data Kursus</h1> 
                    </div>
                    <div class="col-6">
                        <div class="text-end upgrade-btn">
                            @can('course_create')
                                <p >
                                    <a href="{{ route('admin.courses.create') }}" class="btn btn-primary text-white">Tambah Kursus</a>
                                    
                                </p>
                            @endcan
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- adadada -->
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Data Kursus</h4>
                            </div>
                            <div class="table-responsive m-t-20">
                                <table class="table table-bordered table-responsive-lg">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            @if(auth()->user()->isAdmin())
                                                <th scope="col">Nama Guru</th>
                                            @endif
                                            <th scope="col">Judul</th>
                                            <th scope="col">Sub Alamat</th>
                                            <th scope="col">Deskripsi</th>
                                            <th scope="col">Foto Kursus</th>
                                            <th scope="col">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1 ?>
                                        @forelse($courses as $key => $course)
                                            <tr>
                                                <td>{{ $no++ }}</td>
                                                @if(auth()->user()->isAdmin())
                                                    <td>
                                                        @foreach ($course->teachers as $singleTeachers)
                                                            <p>{{ $singleTeachers->name }}</p>
                                                        @endforeach
                                                    </td>
                                                @endif
                                                <td>{{ $course->title ?? '' }}</td>
                                                <td>{{ $course->slug ?? '' }}</td>
                                                <td>{{ $course->description ?? '' }}</td>
                                                <td>
                                                    <img width="150" src="{{ Storage::url($course->course_image) }}" alt="{{ $course->course_image }}">
                                                </td>
                                                <td>
                                                    <a class="btn btn-sm btn-info" href="{{ route('admin.courses.edit', $course->id) }}">
                                                        Edit
                                                    </a>
                                                    <form action="{{ route('admin.courses.destroy', $course->id) }}" method="POST" onsubmit="return confirm('Are you sure ?');" style="display: inline-block;">
                                                        @csrf
                                                        @method('delete')
                                                        <button type="submit" class="btn btn-sm btn-danger" >Delete</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td class="text-center" colspan="5">Tidak Ditemukan Data Kursus</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
        </div>
@endsection