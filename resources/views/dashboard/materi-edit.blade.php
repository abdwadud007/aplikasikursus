@extends('dashboard')

@section('dashboard')
    <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-6">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-0 d-flex align-items-center">
                              <li class="breadcrumb-item"><a href="index.html" class="link"><i class="mdi mdi-home-outline fs-4"></i></a></li>
                              <li class="breadcrumb-item active" aria-current="page">Edit Materi</li>
                            </ol>
                          </nav>
                        <h1 class="mb-0 fw-bold">Edit Materi</h1> 
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-lg-9 col-xlg-9 col-md-7">
                        <div class="card">
                            <div class="card-body">
                                 <form action="{{ route('admin.lessons.update', $lesson->id) }}" method="POST">
                                    @csrf
                                    @method('put')
                                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                            <label for="course_id">Kursus*</label>
                                            <select name="course_id" class="form-control" id="teacher" >
                                                @foreach($courses as $id => $courseName)
                                                    <option {{ $id == $lesson->course_id ? "selected" : null }} value="{{ $id }}">{{ $courseName }}</option>
                                                @endforeach
                                            </select>
                                            @if($errors->has('title'))
                                                <em class="invalid-feedback">
                                                    {{ $errors->first('title') }}
                                                </em>
                                            @endif
                                        </div>
                                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                        <label for="title">Judul*</label>
                                        <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($lesson) ? $lesson->title : '') }}" required>
                                        @if($errors->has('title'))
                                            <em class="invalid-feedback">
                                                {{ $errors->first('title') }}
                                            </em>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                                        <label for="slug">Sub Alamat*</label>
                                        <input type="text" id="slug" name="slug" class="form-control" value="{{ old('slug', isset($lesson) ? $lesson->slug : '') }}" required>
                                        @if($errors->has('slug'))
                                            <em class="invalid-feedback">
                                                {{ $errors->first('slug') }}
                                            </em>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                                        <label for="full_text">Deskripsi Panjang*</label>
                                        <textarea id="desccription" name="full_text" rows="5" class="form-control" required>{{ old('full_text', isset($lesson) ? $lesson->full_text : '') }}</textarea>
                                        @if($errors->has('slug'))
                                            <em class="invalid-feedback">
                                                {{ $errors->first('slug') }}
                                            </em>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                                        <label for="short_text">Deskripsi Pendek*</label>
                                        <textarea id="desccription" name="short_text" rows="5" class="form-control" required>{{ old('short_text', isset($lesson) ? $lesson->short_text : '') }}</textarea>
                                        @if($errors->has('slug'))
                                            <em class="invalid-feedback">
                                                {{ $errors->first('slug') }}
                                            </em>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                                        <label for="embed_id">Link Youtube*</label>
                                        <input type="text" id="embed_id" name="embed_id" class="form-control" value="{{ old('embed_id', isset($lesson) ? $lesson->embed_id : '') }}" required />
                                        @if($errors->has('slug'))
                                            <em class="invalid-feedback">
                                                {{ $errors->first('slug') }}
                                            </em>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                                        <input type="hidden" name="free_lesson" value="1">
                                        @if($errors->has('slug'))
                                            <em class="invalid-feedback">
                                                {{ $errors->first('slug') }}
                                            </em>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                                        <input type="hidden" name="published" value="1">
                                        @if($errors->has('slug'))
                                            <em class="invalid-feedback">
                                                {{ $errors->first('slug') }}
                                            </em>
                                        @endif
                                    </div>
                                    <div>
                                        <button class="btn btn-danger" type="submit" >Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by Flexy Admin. Designed and Developed by <a
                    href="https://www.wrappixel.com">WrapPixel</a>.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
@endsection