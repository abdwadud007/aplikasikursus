@extends('dashboard')

@section('dashboard')
    <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-6">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-0 d-flex align-items-center">
                              <li class="breadcrumb-item"><a href="index.html" class="link"><i class="mdi mdi-home-outline fs-4"></i></a></li>
                              <li class="breadcrumb-item active" aria-current="page">Edit Materi</li>
                            </ol>
                          </nav>
                        <h1 class="mb-0 fw-bold">Edit Materi</h1> 
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-lg-9 col-xlg-9 col-md-7">
                        <div class="card">
                            <div class="card-body">
                                    <form action="{{ route('admin.users.update', $user->id) }}" method="POST">
                                        @csrf
                                        @method('put')
                                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                            <label for="name">Nama Lengkap*</label>
                                            <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($user) ? $user->name : '') }}" required>
                                            @if($errors->has('name'))
                                                <em class="invalid-feedback">
                                                    {{ $errors->first('name') }}
                                                </em>
                                            @endif
                                        </div>
                                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                            <label for="email">Email*</label>
                                            <input type="text" id="email" name="email" class="form-control" value="{{ old('email', isset($user) ? $user->email : '') }}" required>
                                            @if($errors->has('email'))
                                                <em class="invalid-feedback">
                                                    {{ $errors->first('email') }}
                                                </em>
                                            @endif
                                        </div>
                                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                            <label for="password">Password*</label>
                                            <input type="text" id="password" name="password" class="form-control" value="{{ old('password', isset($user) ? $user->password : '') }}" required>
                                            @if($errors->has('password'))
                                                <em class="invalid-feedback">
                                                    {{ $errors->first('password') }}
                                                </em>
                                            @endif
                                        </div>
                                        <div class="form-group {{ $errors->has('role') ? 'has-error' : '' }}">
                                            <label for="role">Role*</label>
                                            <select name="role[]" class="form-control" id="role" multiple >
                                                @foreach($roles as $id => $role)
                                                    <option {{ in_array($id, $user->role->pluck('id')->toArray()) ? 'selected' : null }} value="{{ $id }}">{{ $role }}</option>
                                                @endforeach
                                            </select>
                                            @if($errors->has('role'))
                                                <em class="invalid-feedback">
                                                    {{ $errors->first('role') }}
                                                </em>
                                            @endif
                                        </div>
                                        <div>
                                            <button class="btn btn-danger" type="submit" >Save</button>
                                        </div>
                                    </form>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by Flexy Admin. Designed and Developed by <a
                    href="https://www.wrappixel.com">WrapPixel</a>.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
@endsection