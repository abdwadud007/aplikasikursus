@extends('dashboard')

@section('dashboard')
    <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-6">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-0 d-flex align-items-center">
                              <li class="breadcrumb-item"><a href="index.html" class="link"><i class="mdi mdi-home-outline fs-4"></i></a></li>
                              <li class="breadcrumb-item active" aria-current="page">Edit Kursus</li>
                            </ol>
                          </nav>
                        <h1 class="mb-0 fw-bold">Edit Kursus</h1> 
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-lg-9 col-xlg-9 col-md-7">
                        <div class="card">
                            <div class="card-body">
                                 <form action="{{ route('admin.courses.update', $course->id) }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    @method('put')
                                    @if(!auth()->user()->isAdmin())
                                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                            <label for="teacher">Teachers*</label>
                                            <select name="teachers[]" class="form-control" id="teacher" multiple>
                                                @foreach($teachers as $id => $teacher)
                                                    <option {{ $course->teachers->pluck('id')[0] == $id ? 'selected' : null }} value="{{ $id }}">{{ $teacher }}</option>
                                                @endforeach
                                            </select>
                                            @if($errors->has('title'))
                                                <em class="invalid-feedback">
                                                    {{ $errors->first('title') }}
                                                </em>
                                            @endif
                                        </div>
                                    @endif
                                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                        <label for="title">Judul*</label>
                                        <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($course) ? $course->title : '') }}" required>
                                        @if($errors->has('title'))
                                            <em class="invalid-feedback">
                                                {{ $errors->first('title') }}
                                            </em>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                                        <label for="slug">Alamat Slug*</label>
                                        <input type="text" id="slug" name="slug" class="form-control" value="{{ old('slug', isset($course) ? $course->slug : '') }}" required>
                                        @if($errors->has('slug'))
                                            <em class="invalid-feedback">
                                                {{ $errors->first('slug') }}
                                            </em>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                                        <label for="description">Deskripsi*</label>
                                        <textarea id="description" name="description" rows="5" class="form-control" required>{{ old('description', isset($course) ? $course->description : '') }}</textarea>
                                        @if($errors->has('slug'))
                                            <em class="invalid-feedback">
                                                {{ $errors->first('slug') }}
                                            </em>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                                        <input type="hidden" name="price" value="0">
                                        @if($errors->has('slug'))
                                            <em class="invalid-feedback">
                                                {{ $errors->first('slug') }}
                                            </em>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                                        <label for="course_image">Gambar Kursus*</label>
                                        <input type="file" id="course_image" name="course_image" class="form-control" value="{{ old('course_image', isset($course) ? $course->course_image : '') }}" />
                                        @if($errors->has('slug'))
                                            <em class="invalid-feedback">
                                                {{ $errors->first('slug') }}
                                            </em>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                                        <label for="start_date">Tanggal Upload*</label>
                                        <input type="date" id="start_date" name="start_date" class="form-control" value="{{ old('start_date', isset($course) ? $course->start_date : '') }}" required />
                                        @if($errors->has('slug'))
                                            <em class="invalid-feedback">
                                                {{ $errors->first('slug') }}
                                            </em>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                                        <label for="published">Published*</label>
                                        <input type="hidden" name="published" value="1">
                                        @if($errors->has('slug'))
                                            <em class="invalid-feedback">
                                                {{ $errors->first('slug') }}
                                            </em>
                                        @endif
                                    </div>

                                    <div>
                                        <button class="btn btn-danger" type="submit" >Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by Flexy Admin. Designed and Developed by <a
                    href="https://www.wrappixel.com">WrapPixel</a>.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
@endsection