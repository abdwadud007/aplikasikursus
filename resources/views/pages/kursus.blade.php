@extends('beranda')

@section('content')
    
  <main id="main">

    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog">
      <div class="container" data-aos="fade-up">

        <div class="row gy-4 posts-list">
          <h2 class="section-title">All Course</h2>

          @foreach ($courses as $course)
            <div class="col-xl-4 col-md-6">
              <article>

                <div class="post-img">
                  <img src="{{ Storage::url($course->course_image) }}" alt="" class="img-fluid">
                </div>

                <h2 class="title">
                  <a href="{{ route('courses.show', [$course->slug]) }}">{{ $course->title }}</a>
                </h2>
                @if($course->students()->count() > 5)
                  <button class="products-button">
                    Popular
                  </button>
                @endif
                <span class="products-student">
                {{ $course->students()->count() }} students
                </span>
              </article>
            </div><!-- End post list item -->
          @endforeach
          <div class="mt-lg-5">
                    {{ $courses->links() }}
                </div>
        </div><!-- End blog posts list -->

      </div>
    </section><!-- End Blog Section -->

  </main><!-- End #main -->

@endsection