@extends('beranda')

@section('content')
    <main id="main">
    <!-- ======= Blog Details Section ======= -->
    <section id="blog" class="blog">
      <div class="container" data-aos="fade-up">

        <div class="row g-5">

          <div class="col-lg-8">

            <article class="blog-details">

              <div class="post-img">
                <img src="{{ Storage::url($course->course_image) }}" alt="" class="img-fluid">
              </div>

              <h2 class="title">{{ $course->title }}</h2>

              <div class="content">
                <p>{{ $course->description }}</p>
              </div><!-- End post content -->

            </article><!-- End blog post -->
          </div>

          <div class="col-lg-4">

            <div class="sidebar">
                <div class="sidebar-item">
                  <ul>
                    @foreach ($course->publishedLessons->take(3) as $lesson)
                      <li>
                        @if ($lesson->free_lesson)
                            <a class="lesson-title" href="{{ route('lessons.show', [$lesson->course_id, $lesson->slug]) }}"><i class="bx bx-play-circle"></i>{{ $lesson->title }}</a>
                        @else   
                          @if (!$purchased_course)
                            <a class="lesson-title" aria-disabled="false" style="cursor:  alias" href="#"><i class='bx bx-lock'></i>Materi Lainnya {{ $lesson->count() }}</a>
                          @else  
                            <a class="lesson-title" href="{{ route('lessons.show', [$lesson->course_id, $lesson->slug]) }}"><i class="bx bx-play-circle"></i>{{ $lesson->title }}</a>
                          @endif
                        @endif 
                    </li>
                    @endforeach
                  </ul>
                </div>
                <div class="sidebar-item">
                    
                </div>
                <div class="sidebar-item">
                  @if (auth()->check())
                      @if ($course->students()->where('user_id', auth()->id())->count() == 0)
                          <form action="{{ route('courses.payment') }}" method="POST">
                          @csrf
                              <input type="hidden" name="course_id" value="{{ $course->id }}" />
                              <input type="hidden" name="amount" value="{{ $course->price }}" />
                              <input type="hidden" name="lesson_id" value="{{   $course->publishedLessons[0]->slug }}" />
                              <button class="btn btn-primary">Bayar Kursus</button>
                          </form>
                      @endif
                  @else
                      <a href="{{ route('register') }}?redirect_url={{ route('courses.show', [$course->slug]) }}"
                      class="btn btn-primary" style="text-align: center;">Beli Kursus</a>
                  @endif
                </div><!-- End sidebar search formn-->

            </div><!-- End Blog Sidebar -->

          </div>
        </div>

      </div>
    </section><!-- End Blog Details Section -->

  </main><!-- End #main -->

@endsection