@extends('beranda')

@section('content')
    
    <main id="main">
    <!-- ======= Blog Details Section ======= -->
    <section id="blog" class="blog">
      <div class="container" data-aos="fade-up">

        <div class="row g-5">

          <div class="col-lg-8">

            <article class="blog-details">

              <div class="post-img p-4">
                <iframe src="https://www.youtube.com/embed/{{ $lesson->embed_id }}" width="800" height="500"> </iframe>
              </div>
               @if($purchased_course)
                    @if ($previous_lesson)
                        <div
                        class="swiper-button-prev watch-prev-icon"
                        style="
                            left: 10px;
                            right: initial;
                            bottom: 0px;
                            border-radius: 50%;
                        "
                        >
                        <a href="{{ route('lessons.show', [$previous_lesson->course_id, $previous_lesson->slug]) }}"> <i class="bx bx-left-arrow-alt"></i></a>
                        </div>
                    @endif
                    @if ($next_lesson)
                        <div
                        class="swiper-button-next watch-next-icon"
                        style="
                            right: 10px;
                            left: initial;
                            bottom: 0px;
                            border-radius: 50%;
                        "
                        >
                        <a href="{{ route('lessons.show', [$next_lesson->course_id, $next_lesson->slug]) }}"><i class="bx bx-right-arrow-alt"></i></a>
                        </div>
                    @endif
                @endif
            </article><!-- End blog post -->
          </div>

          <div class="col-lg-4">

            <div class="sidebar">
                <div class="sidebar-item">
                  <ul>
                    @foreach ($lesson->course->publishedLessons as $publishedLesson)
                        @if ($publishedLesson->free_lesson)
                            <li>
                                <a href="{{ route('lessons.show', [$publishedLesson->course_id, $publishedLesson->slug]) }}"><i class="bx bx-play-circle"></i>{{ $publishedLesson->title }}</a>
                            </li>
                        @else
                        @if($purchased_course)
                            <li>
                                <a href="{{ route('lessons.show', [$publishedLesson->course_id, $publishedLesson->slug]) }}"><i class="bx bx-play-circle"></i>{{ $publishedLesson->title }}</a>
                            </li>
                        @endif
                        @endif
                    @endforeach
                  </ul>
                  @if(!$purchased_course)
                        @if (auth()->check())
                            @if ($lesson->course->students()->where('user_id', auth()->id())->count() == 0)
                                <form action="{{ route('courses.payment') }}" style="margin-top: 1rem;" method="POST">
                                @csrf
                                    <input type="hidden" name="course_id" value="{{ $lesson->course->id }}" />
                                    <input type="hidden" name="amount" value="{{ $lesson->course->price }}" />
                                    <input type="hidden" name="lesson_id" value="{{   $lesson->course->publishedLessons[0]->slug }}" />
                                    <button class="button detail-button">Purchase Course</button>
                                </form>
                            @endif
                        @else
                            <a href="{{ route('register') }}?redirect_url={{ route('courses.show', [$lesson->course->slug]) }}"
                            class="button detail-button" style="text-align: center;">Purchase Course (${{ $lesson->course->price }})</a>
                        @endif
                    @endif
                </div>
                <div class="sidebar-item">
                    
                </div>
                <div class="sidebar-item">
                 
                </div><!-- End sidebar search formn-->

            </div><!-- End Blog Sidebar -->

          </div>
        </div>

      </div>
    </section><!-- End Blog Details Section -->

  </main><!-- End #main -->

@endsection