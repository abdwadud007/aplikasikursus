@extends('beranda')

@section('content')
    
  <main id="main">

    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog">
      <div class="container" data-aos="fade-up">

        <div class="row gy-4 posts-list">
          <h2 class="section-title">Kursus Saya</h2>

          @foreach ($purchased_courses as $pcourse)
            <div class="col-xl-4 col-md-6">
              <article>

                <div class="post-img">
                  <img src="{{ Storage::url($pcourse->course_image) }}" alt="" class="img-fluid">
                </div>

                <h2 class="title">
                  <a href="{{ route('courses.show', [$pcourse->slug]) }}">{{ $pcourse->title }}</a>
                </h2>
                @if($pcourse->students()->count() > 5)
                  <button class="products-button">
                    Popular
                  </button>
                @endif
                <span class="products-student">
                {{ $pcourse->students()->count() }} students
                </span>
              </article>
            </div><!-- End post list item -->
          @endforeach
          <div class="mt-lg-5">
                    {{-- {{ $pcourse->links() }} --}}
                </div>
        </div><!-- End blog posts list -->

      </div>
    </section><!-- End Blog Section -->

  </main><!-- End #main -->

@endsection