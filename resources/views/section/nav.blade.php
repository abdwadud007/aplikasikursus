
  <header id="header" class="header d-flex align-items-center">

    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">
      <a href="index.html" class="logo d-flex align-items-center">
        <!-- Uncomment the line below if you also wish to use an image logo -->
        <!-- <img src="assets/img/logo.png" alt=""> -->
        <h1>BelajarBarengKami<span>.</span></h1>
      </a>
      <nav id="navbar" class="navbar">
        <ul>
          <li><a href="/">Beranda</a></li>
          <li><a href="/kursus">Kursus</a></li>
          @auth
            @if(auth()->user()->isAdmin())
              <li class="nav-item">
                <a href="{{ route('admin.courses.index') }}" class="nav-link">Admin</a>
              </li> 
            @endif
          @endauth
          @auth
            <li>
            <a
                href="{{ route('courses.pindex') }}"
                class="button nav-link"
                >Kursus Saya</a
            >
            </li>
            <li class="dropdown"><a href="#"><span>{{ auth()->user()->name }}</span> <i class="bi bi-chevron-down dropdown-indicator"></i></a>
                <ul>
                  <li>
                    <a
                        href="#"
                        class="button nav-link"
                        onclick="getElementById('logout').submit()"
                        >Logout</a
                    >
                    <form id="logout" action="{{ route('logout') }}" method="post">
                        @csrf
                    </form>
                  </li>
                </ul>
              </li>
            <li>
            
            </li>
         @endauth
        @guest 
            <li>
            <a
                href="{{ route('login') }}"
                class="button nav-link"
                >Login</a
            >
            </li>
            <li>
            <a
                href="{{ route('register') }}"
                class="button nav-link"
                >Daftar</a
            >
            </li>
         @endguest
        </ul>
      </nav><!-- .navbar -->

      <i class="mobile-nav-toggle mobile-nav-show bi bi-list"></i>
      <i class="mobile-nav-toggle mobile-nav-hide d-none bi bi-x"></i>

    </div>
  </header><!-- End Header -->