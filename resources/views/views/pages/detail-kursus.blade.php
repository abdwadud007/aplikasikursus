@extends('index')

@section('content')
    <main id="main">


    <!-- ======= Blog Details Section ======= -->
    <section id="blog" class="blog">
      <div class="container" data-aos="fade-up">

        <div class="row g-5">

          <div class="col-lg-8">

            <article class="blog-details">

              <div class="post-img">
                <img src="{{ url('/data_gambar/'. $courseBlog->gambarcourse)}}" alt="" class="img-fluid">
              </div>

              <h2 class="title">{{ $courseBlog->namacourse }}</h2>

              <div class="meta-top">
                <ul>
                  <li class="d-flex align-items-center"><i class="bi bi-person"></i> <a href="blog-details.html">{{ $courseBlog->user->name }}</a></li>
                  <li class="d-flex align-items-center"><i class="bi bi-clock"></i> <a href="#"><time>{{ $courseBlog->created_at }}</time></a></li>
                  <li class="d-flex align-items-center"><i class="bi bi-chat-dots"></i> <a href="#">12 Murid</a></li>
                </ul>
              </div><!-- End meta top -->

              <div class="content">
                <p>{{ $courseBlog->descourse }}</p>
              </div><!-- End post content -->

              {{-- <div class="meta-bottom">
                <i class="bi bi-folder"></i>
                <ul class="cats">
                  <li><a href="#">Business</a></li>
                </ul>

                <i class="bi bi-tags"></i>
                <ul class="tags">
                  <li><a href="#">Creative</a></li>
                  <li><a href="#">Tips</a></li>
                  <li><a href="#">Marketing</a></li>
                </ul>
              </div><!-- End meta bottom --> --}}

            </article><!-- End blog post -->
          </div>

          <div class="col-lg-4">

            <div class="sidebar">
                <div class="sidebar-item">
                    <p>Tingkat Kesusahan : 
                        @if ($courseBlog->levelcourse == "Susah")
                            <button disabled="disabled" class="btn btn-sm btn-danger">{{ $courseBlog->levelcourse }}</button>
                        @endif
                        @if ($courseBlog->levelcourse == "Mudah")
                            <button disabled="disabled" class="btn btn-sm btn-primary">{{ $courseBlog->levelcourse }}</button>
                        @endif
                        @if ($courseBlog->levelcourse == "Menengah")
                            <button disabled="disabled" class="btn btn-sm btn-warning">{{ $courseBlog->levelcourse }}</button>
                        @endif
                        @if ($courseBlog->levelcourse == "Profesional")
                            <button disabled="disabled" class="btn btn-sm btn-dark">{{ $courseBlog->levelcourse }}</button>
                        @endif
                    </p>
                </div>
                <div class="sidebar-item">
                    <p>Ratings :</p>
                    <p>
                        Bagus [1] || Jelek [0]
                    </p>
                </div>
                <div class="sidebar-item">
                    <form action="" method="post">
                        <input type="hidden">
                        <div class="row d-flex justify-content-center">
                            <button type="submit" class="btn btn-lg btn-primary" >
                                GABUNG KELAS
                            </button>

                        </div>
                    </form>
                </div><!-- End sidebar search formn-->

            </div><!-- End Blog Sidebar -->

          </div>
        </div>

      </div>
    </section><!-- End Blog Details Section -->

  </main><!-- End #main -->

@endsection