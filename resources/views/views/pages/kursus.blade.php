@extends('index')

@section('content')
    
  <main id="main">

    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog">
      <div class="container" data-aos="fade-up">

        <div class="row gy-4 posts-list">
          @foreach ($course as $c)
            <div class="col-xl-4 col-md-6">
              <article>

                <div class="post-img">
                  <img src="{{ url('/data_gambar/'.$c->gambarcourse) }}" alt="" class="img-fluid">
                </div>

                <p class="post-category">{{ $c->levelcourse }}</p>

                <h2 class="title">
                  <a href="{{ url('detail-kursus/'.$c->id) }}">{{ $c->namacourse }}</a>
                </h2>

                <div class="d-flex align-items-center">
                  <img src="{{ url('/data_photo_user/'. $c->user->fotouser) }}"
                                                class="rounded-circle post-author-img flex-shrink-0" width="50" height="50" />
                  <div class="post-meta">
                    <p class="post-author-list">{{ $c->user->name }}</p>
                    <p class="post-date">
                      <time datetime="2022-01-01">{{ $c->created_at }}</time>
                    </p>
                  </div>
                </div>

              </article>
            </div><!-- End post list item -->
          @endforeach
        </div><!-- End blog posts list -->

        {{-- <div class="blog-pagination">
          <ul class="justify-content-center">
            <li><a href="#">1</a></li>
            <li class="active"><a href="#">2</a></li>
            <li><a href="#">3</a></li>
          </ul>
        </div><!-- End blog pagination --> --}}

      </div>
    </section><!-- End Blog Section -->

  </main><!-- End #main -->

@endsection