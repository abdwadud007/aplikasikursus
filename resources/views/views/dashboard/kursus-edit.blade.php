@extends('dashboard')

@section('dashboard-content')
       <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-6">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-0 d-flex align-items-center">
                              <li class="breadcrumb-item"><a href="/dashboard" class="link"><i class="mdi mdi-home-outline fs-4"></i></a></li>
                              <li class="breadcrumb-item active" aria-current="page">Tambah Kursus</li>
                            </ol>
                          </nav>
                        <h1 class="mb-0 fw-bold">Tambah Kursus</h1> 
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                    <form action="{{ url('kursus-edit-simpan/'. $eCourse->id) }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                        <div class="row">
                            <div class="col-lg col-xlg-9 col-md-7">
                                <div class="card">
                                    <div class="card-body">
                                        <form class="form-horizontal form-material mx-2">
                                            <div class="form-group">
                                                <label for="" class="col-md-12">Foto Kursus</label>
                                                 <img src="{{ url('/data_gambar/'. $eCourse->gambarcourse) }}"
                                                class="box" width="300" height="190"/>
                                                <input type="file" name="gambarcourse" value="{{ $eCourse->gambarcourse }}">
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">Nama Kursus</label>
                                                <div class="col-md-12">
                                                    <input type="text" placeholder="Nama kursus anda"
                                                        class="form-control form-control-line" name="namacourse" value="{{ $eCourse->namacourse }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="example-email" class="col-md-12">Tingkat Kesulitan</label>
                                                <div class="col-md-12">
                                                    <input type="hidden" name="levelcourse" id="levelcourse">
                                                    <select name="levelcourse" id="levelcourse" class="form-control">
                                                        <option value="Mudah">Mudah</option>
                                                        <option value="Menengah">Menengah</option>
                                                        <option value="Susah">Susah</option>
                                                        <option value="Profesional">Profesional</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">Deskripsi Kursus</label>
                                                <div class="col-md-12">
                                                    <textarea rows="5" class="form-control form-control-line" name="descourse" placeholder="Deskripsi Kursus anda">{{ $eCourse->descourse }}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <button class="btn btn-success text-white" type="submit">Edit Kursus</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Column -->
                        </div>

                    </form>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
   
        </div>
@endsection