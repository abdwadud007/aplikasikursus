@extends('dashboard')

@section('dashboard-content')
       <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-6">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-0 d-flex align-items-center">
                              <li class="breadcrumb-item"><a href="index.html" class="link"><i class="mdi mdi-home-outline fs-4"></i></a></li>
                              <li class="breadcrumb-item active" aria-current="page">Profile</li>
                            </ol>
                          </nav>
                        <h1 class="mb-0 fw-bold">Profile</h1> 
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                    <form action="/profile-simpan" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                        <div class="row">
                            <!-- Column -->
                            <div class="col-lg-4 col-xlg-3 col-md-5">
                                <div class="card">
                                    <div class="card-body">
                                        <center class="m-t-30"> <img src="{{ url('/data_photo_user/'. Auth::user()->fotouser) }}"
                                                class="rounded-circle" width="150" height="150"/>
                                            <h4 class="card-title m-t-10">{{ Auth::user()->name }}</h4>
                                            <h6 class="card-subtitle">{{ Auth::user()->email }}</h6>
                                            <div class="row text-center justify-content-md-center">
                                                <div class="col-4"><a href="javascript:void(0)" class="link"><i
                                                            class="icon-people"></i>
                                                        <font class="font-medium">254 Course</font>
                                                    </a></div>
                                        </center>
                                    </div>
                                    <div>
                                        <hr>
                                    </div>
                                    <div class="card-body"> <small class="text-muted">Email address </small>
                                        <h6>{{ Auth::user()->email }}</h6> <small class="text-muted p-t-30 db">Roles</small>
                                        <h6>{{ Auth::user()->roles }}</h6>
                                    </div>
                                </div>
                            </div>
                            <!-- Column -->
                            <!-- Column -->
                            <div class="col-lg-8 col-xlg-9 col-md-7">
                                <div class="card">
                                    <div class="card-body">
                                        <form class="form-horizontal form-material mx-2">
                                            <div class="form-group">
                                                <label for="" class="col-md-12">Ganti Foto</label>
                                                <input type="file" name="fotouser">
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">Nama Lengkap</label>
                                                <div class="col-md-12">
                                                    <input type="text" placeholder="Isi Nama Lengkap Anda"
                                                        class="form-control form-control-line" value="{{ Auth::user()->name }}" name="name">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="example-email" class="col-md-12">Email</label>
                                                <div class="col-md-12">
                                                    <input type="email" placeholder="Cth : kamu@gmail.com"
                                                        class="form-control form-control-line"
                                                        id="example-email" value="{{ Auth::user()->email }}" name="email">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">Password</label>
                                                <div class="col-md-12">
                                                    <input type="password" placeholder="Password Kamu"
                                                        class="form-control form-control-line" name="password" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">Bio</label>
                                                <div class="col-md-12">
                                                    <textarea rows="5" class="form-control form-control-line" name="bio">{{ Auth::user()->bio }}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <button class="btn btn-success text-white" type="submit">Update Profile</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Column -->
                        </div>

                    </form>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
   
        </div>
@endsection