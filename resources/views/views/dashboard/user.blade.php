@extends('dashboard')

@section('dashboard-content')
    <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-6">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-0 d-flex align-items-center">
                              <li class="breadcrumb-item"><a href="index.html" class="link"><i class="mdi mdi-home-outline fs-4"></i></a></li>
                              <li class="breadcrumb-item active" aria-current="page">Data User</li>
                            </ol>
                          </nav>
                        <h1 class="mb-0 fw-bold">Data User</h1> 
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row">
                    <!-- column -->
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <!-- title -->
                                <div class="d-md-flex">
                                    <div>
                                        <h4 class="card-title">Data User</h4>
                                    </div>
                                    <div class="ms-auto">
                                        <div class="dl">
                                            <button class="btn btn-primary">Tambah User</button>
                                        </div>
                                    </div>
                                </div>
                                <!-- title -->
                                <div class="table-responsive">
                                    <table class="table mb-0 table-hover align-middle text-nowrap">
                                        <thead>
                                            <tr>
                                                <th class="border-top-0">Nama</th>
                                                <th class="border-top-0">Email</th>
                                                <th class="border-top-0">Roles</th>
                                                <th class="border-top-0">Opsi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($user as $item)
                                                <tr>
                                                    <td>
                                                        <div class="d-flex align-items-center">
                                                            <div class="m-r-10">
                                                                    <img src="{{ url('/data_photo_user/'. $item->fotouser) }}"
                                                class="rounded-circle" width="30" height="30"/>
                                                            </div>
                                                            <div class="">
                                                                <h4 class="m-b-0 font-16">{{ $item->name }}</h4>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>{{ $item->email }}</td>
                                                    <td>
                                                        <label class="badge bg-primary">{{ $item->roles }}</label>
                                                    </td>
                                                    <td>
                                                        <a href="Edit">Edit</a>
                                                        <a href="">Hapus</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
        </div>
@endsection