<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Course Upload</title>
</head>
<body>
    <form action="materi-simpan" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{-- <input type="file" name="gambarcourse" required> --}}
        <input type="text" name="id_course" placeholder="id Course" required>
        <input type="text" name="namamateri" placeholder="Nama Course" required>
        <input type="text" name="linkvideo" placeholder="link Course" required>
        <input type="text" name="descmateri" placeholder="desc Course" required>
        <button type="submit">Buat Course</button>
    </form>
</body>
</html>